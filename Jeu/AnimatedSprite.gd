extends AnimatedSprite

var _rolling = true

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var rng = RandomNumberGenerator.new()
	var val = 0
	var prev = 0
	# We set the different frames of the dice in order to make it move.
	while(_rolling):
		var t = Timer.new()
		t.set_wait_time(0.2)
		t.set_one_shot(false)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		rng.randomize()
		while(val==prev):
			val = rng.randi_range(0,5)
		set_frame(val)
		prev = val

func set_rolling(rolling: bool) -> void:
	_rolling=rolling

func set_dice(val: int) -> void:
	set_frame(val-1)
