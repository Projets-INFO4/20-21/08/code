extends Node2D
class_name ShakerDice

var m_Dice:Dice
var angle:float
var speedx:float
var speedy:float
var rolling:bool

const POS = 0
const SIZE = 350
const SPEED = 400
var i = 0

"""
This function is called whent the ShakerDice is coming on the scene tree.
It puts the Dice in the center of the window.
"""
func _ready() -> void:
	rolling = true
	m_Dice = get_child(0)
	m_Dice.position= Vector2(POS,POS)
#	var rng = RandomNumberGenerator.new()
#	rng.randomize()
#	angle = rng.randf_range(0,2*PI)
#	speedx = cos(angle)*SPEED
#	speedy = sin(angle)*SPEED

"""
func distance_center(delta: float) -> void:
	if(rolling):
		var dist_x = m_Dice.position.x
		var dist_y = m_Dice.position.y
		var dist = sqrt(dist_x*dist_x+dist_y*dist_y)
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		if(dist>SIZE/2):
			angle = rng.randf_range(angle-(PI/2)-(PI/8), angle-(PI/2)+(PI/8))
			if angle > 2*PI:
				angle -= 2*PI
			if angle < 0:
				angle += 2*PI
			speedx = cos(angle)*SPEED
			speedy = sin(angle)*SPEED
		m_Dice.position.x += speedx*delta
		m_Dice.position.y += speedy*delta
"""

func _physics_process(delta: float) -> void:
	i +=  delta
	if(i > 2):
		if(rolling):
			m_Dice.prepare_dice()
		i = 0

"""
This function is called when a player touches the table in order to display the number which correspond to the number of cases to move on.
"""
func set_dice(val: int) -> void:
	if(rolling):
		rolling=false
	m_Dice.set_dice(val)
	var t = Timer.new()
	t.set_wait_time(2)
	t.set_one_shot(false)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	set_rolling(true)
	pass

func set_rolling(roll: bool) ->void:
	m_Dice.set_rolling(roll)
	rolling = roll
	pass
