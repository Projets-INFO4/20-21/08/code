extends Node2D
class_name Board

puppet var Cells_matrice = []
var players = []
var current_player = 0
var Game
var winner

var index_current_player = 0
var blocked_cells = []
var effectShower:effectShower

"""
This function is called when the server creates the board.
"""
func creation(game: Game) -> String:
	Game = game
	var res = generate_tile(game)
	
	players = Game.players
	
	# For each player we put the piece on the first cell.
	for i in range(players.size()):
		add_child(players[i])
		players[i].position = Vector2(0,0)
		players[i].set_board(self)
		players[i].set_cell(Cells_matrice[0])
	effectShower = load("res://Ressources/effectshower.tscn").instance()
	effectShower.position = Vector2(800,800)
	effectShower.hide()
	add_child(effectShower)
	return res

"""
This function is called when the client creates the board thanks to the informations from the server.
"""
func creation_client(game: Game, res: String) -> void:
	Game = game
	
	var tab = res.split(" ") # We collect the list of cells given by the server.
	generate_tiles_client(game, tab)
	
	players = Game.players
	
	# For each player we put the piece on the first cell.
	for i in range(players.size()):
		add_child(players[i])
		players[i].position = Vector2(0,0)
		players[i].set_board(self)
		players[i].set_cell(Cells_matrice[0])
	effectShower = load("res://Ressources/effectshower.tscn").instance()
	effectShower.position = Vector2(800,800)
	effectShower.hide()
	add_child(effectShower)

"""
This function is called by creation_client() in order to generate tiles for the board for the clients.
"""
func generate_tiles_client(game: Game, tab: Array) -> void:
	var RedCell = load("res://Ressources/Cell/RedCell.tscn")
	var BlackCell = load("res://Ressources/Cell/BlackCell.tscn")
	var TurquoiseCell = load("res://Ressources/Cell/TurquoiseCell.tscn")
	var GoldCell = load("res://Ressources/Cell/GoldCell.tscn")
	var tile
	var j = 0
	# We load a BlackCell as a first cell because we can't have a TurquoiseCell or a RedCell as a first cell.
	tile = BlackCell.instance()
	add_child(tile)
	Cells_matrice.append(tile)
	tile.set_game(game)
	tile.position = Vector2(1472,1473)
	j += 1
	# Then we load each cells thanks to Array : tab
	for i in range(1, 11):
		if tab[j] == "red": # If we had a redCell on the board of the server we load a redCell.
			tile = RedCell.instance()
		if tab[j] == "black": # Same for a blackCell.
			tile = BlackCell.instance()
		if tab[j] == "turquoise": # Same for a TurquoiseCell.
			tile = TurquoiseCell.instance()
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(1472-i*128,1473)
		j += 1
	# We load each row in the same way.
	for i in range(11):
		if tab[j] == "red":
			tile = RedCell.instance()
		if tab[j] == "black":
			tile = BlackCell.instance()
		if tab[j] == "turquoise":
			tile = TurquoiseCell.instance()
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(64,1472-i*128)
		j += 1
	for i in range(10):
		if tab[j] == "red":
			tile = RedCell.instance()
		if tab[j] == "black":
			tile = BlackCell.instance()
		if tab[j] == "turquoise":
			tile = TurquoiseCell.instance()
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(64+i*128,64)
		j += 1
	for i in range(8):
		if tab[j] == "red":
			tile = RedCell.instance()
		if tab[j] == "black":
			tile = BlackCell.instance()
		if tab[j] == "turquoise":
			tile = TurquoiseCell.instance()
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(1344,64+i*128)
		j += 1
	for i in range(7):
		if tab[j] == "red":
			tile = RedCell.instance()
		if tab[j] == "black":
			tile = BlackCell.instance()
		if tab[j] == "turquoise":
			tile = TurquoiseCell.instance()
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(1344-i*128,1088)
		j += 1
	for i in range(5):
		if tab[j] == "red":
			tile = RedCell.instance()
		if tab[j] == "black":
			tile = BlackCell.instance()
		if tab[j] == "turquoise":
			tile = TurquoiseCell.instance()
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(448,1088-i*128)
		j += 1
	for i in range(5):
		if tab[j] == "red":
			tile = RedCell.instance()
		if tab[j] == "black":
			tile = BlackCell.instance()
		if tab[j] == "turquoise":
			tile = TurquoiseCell.instance()
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(448+i*128,448)
		j += 1
	for i in range(2):
		if tab[j] == "red":
			tile = RedCell.instance()
		if tab[j] == "black":
			tile = BlackCell.instance()
		if tab[j] == "turquoise":
			tile = TurquoiseCell.instance()
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(1088,448+i*128)
		j += 1
	
	# The last cell is a goldCell because it is the end of the board.
	tile = GoldCell.instance()
	add_child(tile)
	Cells_matrice.append(tile)
	tile.set_game(game)
	tile.position = Vector2(1088,704)

"""
This function is called by creation() in order to generate tiles for the board for the server.
"""
func generate_tile(game: Game) -> String:
	var res = ""
	var GoldCell = load("res://Ressources/Cell/GoldCell.tscn")
	var tile:Cell
	var tab
	var BlackCell = load("res://Ressources/Cell/BlackCell.tscn")
	# We load a BlackCell as a first cell because we can't have a TurquoiseCell or a RedCell as a first cell.
	tile = BlackCell.instance()
	add_child(tile)
	Cells_matrice.append(tile)
	tile.set_game(game)
	tile.position = Vector2(1472,1473)
	res += "black "
	for i in range(1, 11):
		# We get a random tile in order to generate a different board each game.
		tab = get_random_tile()
		tile = tab[0]
		res += tab[1] 
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(1472-i*128,1473)
	for i in range(11):
		tab = get_random_tile()
		tile = tab[0]
		res += tab[1]
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(64,1472-i*128)
	for i in range(10):
		tab = get_random_tile()
		tile = tab[0]
		res += tab[1]
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(64+i*128,64)
	for i in range(8):
		tab = get_random_tile()
		tile = tab[0]
		res += tab[1]
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(1344,64+i*128)
	for i in range(7):
		tab = get_random_tile()
		tile = tab[0]
		res += tab[1]
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(1344-i*128,1088)
	for i in range(5):
		tab = get_random_tile()
		tile = tab[0]
		res += tab[1]
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(448,1088-i*128)
	for i in range(5):
		tab = get_random_tile()
		tile = tab[0]
		res += tab[1]
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(448+i*128,448)
	for i in range(2):
		tab = get_random_tile()
		tile = tab[0]
		res += tab[1]
		add_child(tile)
		Cells_matrice.append(tile)
		tile.set_game(game)
		tile.position = Vector2(1088,448+i*128)
	
	# The last cell is a goldCell because it is the end of the board.
	tile = GoldCell.instance()
	add_child(tile)
	res += "gold"
	Cells_matrice.append(tile)
	tile.set_game(game)
	tile.position = Vector2(1088,704)
	return res

"""
This function generates a random cell between RedCell, BlackCell and TurquoiseCell.
"""
func get_random_tile() -> Array:
	var RedCell = load("res://Ressources/Cell/RedCell.tscn")
	var BlackCell = load("res://Ressources/Cell/BlackCell.tscn")
	var TurquoiseCell = load("res://Ressources/Cell/TurquoiseCell.tscn")
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var my_random_number = rng.randi_range(0,100) # We generate a random number between 0 and 100 in order to generate a random cell.
	var res_tile:Cell
	if(my_random_number<50):
		res_tile = BlackCell.instance()
		return [res_tile, "black "] # This function returns an array with the tile and a string in order to generate the list of cells for clients.
	if(my_random_number>=50 && my_random_number<75):
		res_tile = RedCell.instance()
		return [res_tile, "red "]
	if(my_random_number>=75):
		res_tile = TurquoiseCell.instance()
		return [res_tile, "turquoise "]
	return []

"""
This function is called when we need to know the coordinates of a given index number.
"""
func get_cell_coordonnees(index: int) -> Vector2:
	return Cells_matrice[index].position

"""
This function is called when we need to know the cell of a given index number.
"""
func get_cell(index: int) -> Cell:
	return Cells_matrice[index]
