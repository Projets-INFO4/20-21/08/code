extends Node2D
class_name Piece

enum State {
	IDLE,ARMED
}

var state
var indiceCell:int
var CurrentCell:Cell
var Board
var color_player
var change_dice_value = 0
var next_penalty_ignored = false
var id = 1

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	indiceCell=0
	state = State.IDLE
	pass

func set_cell(new_Cell: Cell) -> void:
	CurrentCell = new_Cell
	CurrentCell.add_player(self)
	self.position = new_Cell.position
	return

func set_board(new_board: Board) -> void:
	Board = new_board
	return

"""
This function is called when the player has to move on the board.
'val' represents the number of cases that the player has to move on.
"""
func move(val: int) -> void:
	if(indiceCell+val >= Board.Cells_matrice.size()):
		indiceCell = Board.Cells_matrice.size()-1
	else:
		indiceCell = (indiceCell+val)%Board.Cells_matrice.size()
	var newCell = Board.get_cell(indiceCell)
	if newCell.blocked == false: # If the new Cell is not blocked, the player is moved to the new cell.
		# We remove the player from the actual cell and then we add it to the new cell.
		CurrentCell.remove_player()
		CurrentCell = newCell
		position = CurrentCell.position
		CurrentCell.add_player(self)
	return

func get_state() -> int:
	return state

func change_cell(new_cell: Cell) -> void:
	if new_cell.blocked == false:
		CurrentCell.remove_player()
		CurrentCell = new_cell
		position = CurrentCell.position
		CurrentCell.add_player(self)
	return
