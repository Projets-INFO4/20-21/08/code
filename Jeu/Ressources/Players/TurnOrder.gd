extends Node2D
class_name TurnOrder

var Board
var players = []
var current_player
var index_current_player = 0

func _ready() -> void:
	var player = load("res://Ressources/Players/BluePiece.tscn").instance()
	players.append(player)
	player = load("res://Ressources/Players/BrownPiece.tscn").instance()
	players.append(player)
	draw_TurnOrder()

func set_TurnOrder(board:Board) -> void:
	Board = board
	players = Board.players
	current_player = Board.current_player
	index_current_player = 0

func draw_TurnOrder() -> void:
	for i in range(players.size()):
		var case = load("res://Ressources/Players/TurnOrderCase.tscn").instance()
		add_child(case)
		case.position = Vector2(0,32*i+1)
		case.z_index = i
		add_child(players[i])
		players[i].position = Vector2(0,32*i+1)
		players[i].z_index = i+1

func get_current_player() -> Piece:
	return players[index_current_player]

func end_of_turn() -> void:
	index_current_player += 1
	current_player = get_current_player()
