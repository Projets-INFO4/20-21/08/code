extends AnimatedSprite

class_name effectShower

func sup_set_frame(frame: int) -> void:
	show()
	set_frame(frame)
	var t = Timer.new()
	t.set_wait_time(2)
	t.set_one_shot(false)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	hide()
