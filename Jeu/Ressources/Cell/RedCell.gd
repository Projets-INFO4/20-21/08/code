extends Cell
class_name RedCell

"""
This function is called when a new player come on the cell and it applies the effect of the cell.
"""
func add_player(player: Piece) -> void:
	_number_of_player += 1
	if player.state == player.State.IDLE: # If the player had not received an another bonus or penalty during this turn
		# the effect is applying to this player
		if player.next_penalty_ignored == false:
			apply_effect(player)
		else:
			player.next_penalty_ignored = false
	return

func apply_effect(player: Piece) -> void:
	var t = Timer.new()
	t.set_wait_time(1)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	
	player.state = player.State.ARMED
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var val = randi()%100+1 # We take a number between 1 and 100 in order to apply a random penalty to the player
	
	if(val > 67):
		get_parent().effectShower.sup_set_frame(4)
		var rng2 = RandomNumberGenerator.new()
		rng2.randomize()
		var val2 = randi()%5+1
		player.move(-val2)
		print("Player moved : "+str(val2))
	
	if(val > 34 and val <= 67):
		get_parent().effectShower.sup_set_frame(5)
		var rng2 = RandomNumberGenerator.new()
		rng2.randomize()
		var val2 = randi()%3+1
		player.change_dice_value = -val2
		print("Dice value changed")
	
	if(val > 1 and val <= 34):
		get_parent().effectShower.sup_set_frame(6)
		var rng2 = RandomNumberGenerator.new()
		rng2.randomize()
		var x = randi()%game.Board.Cells_matrice.size()
		
		game.Board.get_cell(x).blocked = true
		game.Board.blocked_cells.append([game.Board.get_cell(x),game.Board.players.size()])
		
		print("Cell blocked")
	
	if(val == 1):
		get_parent().effectShower.sup_set_frame(7)
		player.set_cell(game.Board.Cells_matrice[0])
		print("Player's cell changed")
	player.state = player.State.IDLE
	return
