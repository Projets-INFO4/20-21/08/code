extends Node2D
class_name Cell

var _number_of_player = 0
var game
var blocked = false

func set_game(newGame: Game) -> void:
	game = newGame

func remove_player() -> void:
	if(_number_of_player > 0):
		_number_of_player -= 1
	else:
		push_error("There is an error")
