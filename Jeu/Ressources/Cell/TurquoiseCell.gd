extends Cell
class_name TurquoiseCell

func add_player(player: Piece) -> void:
	_number_of_player += 1
	if player.state == player.State.IDLE:
		apply_effect(player)
	return

func apply_effect(player: Piece) -> void:
	var t = Timer.new()
	t.set_wait_time(1)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	
	player.state = player.State.ARMED
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var val = randi()%3+1 # We take a number between 1 and 3 in order to apply one of the three bonus.
	if(val == 1):
		get_parent().effectShower.sup_set_frame(0)
		var rng2 = RandomNumberGenerator.new()
		rng2.randomize()
		var val2 = randi()%5+1
		player.move(val2)
		print("Player moved : "+str(val2))
	if(val == 2):
		get_parent().effectShower.sup_set_frame(1)
		var rng2 = RandomNumberGenerator.new()
		rng2.randomize()
		var val2 = randi()%3+1
		player.change_dice_value = val2
		print("Dice value changed")
	if(val == 3):
		get_parent().effectShower.sup_set_frame(3)
		player.next_penalty_ignored = true
		print("Next penalty ignored")
	
	player.state = player.State.IDLE
	return
