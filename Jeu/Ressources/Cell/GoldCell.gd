extends Cell
class_name GoldCell

"""
This function is called when a new player come on the cell and it applies the effect of the cell.
"""
func add_player(player: Piece) -> void:
	_number_of_player += 1
	apply_effect(player)
	return

func apply_effect(player: Piece) -> void:
	var t = Timer.new()
	t.set_wait_time(1)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	
	game.end_game()
	return
