extends Cell
class_name BlackCell

"""
This function is called when a new player come on the cell.
"""
func add_player(player: Piece) -> void:
	_number_of_player += 1
	return

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
