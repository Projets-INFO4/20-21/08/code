extends Node

var is_server

var map_id_with_players = {}
var map_id_with_table = {}
var map_id_with_nb = {}

const MAX_PLAYER_COUNT = 7
var players_ready_count = 0

signal game_ready
signal touched_here
var game_loading = true

var client = true
var nb_players_end = 0
var nb_players = 0

"""
This function is useful for the server to know exactly how many players are in the game.
"""
sync func set_players(nb_player: int) -> void:
	print(nb_players)
	if(is_server):
		nb_players += nb_player

"""
This function is called in order to manage easily the different numbers of players :
	- nb_players_end : the number of players which each table entered
	- nb_players : the sum of all players in the game
"""
sync func set_players_end(nb_player: int) -> void:
	nb_players = nb_player
	nb_players_end = nb_player

sync func set_players_server(nb_player: int) -> void:
	print(nb_players)
	nb_players = nb_player
	set_players_(nb_players, 0, get_tree().get_network_unique_id())

"""
This function is called when the game starts in solo.
"""
func start_solo(nb_player: int) -> void:
	create_player_solo(nb_player)

"""
This function creates a Network and launches the server.
"""
func start(as_server = false) -> void:
	is_server = as_server
	
	var peer = NetworkedMultiplayerENet.new()
	
	if(is_server):
		peer.create_server(54321, 2)
		print(IP.get_local_addresses())
		
		create_player(1)
	else:
		peer.create_client("localhost", 54321)
	
	get_tree().network_peer = peer
	get_tree().connect("connected_to_server", self, "_on_connected_to_server")
	get_tree().connect("connection_failed", self, "_on_connection_failed")
	get_tree().connect("network_peer_connected", self, "_on_network_peer_connected") 
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")

"""
This function creates a Network and launches the client.
"""
func start_client(as_server = false, ip = "localhost") -> void:
	is_server = as_server
	
	var peer = NetworkedMultiplayerENet.new() # We create a new peer in order to connect this Lobby to the network.
	
	if(is_server):
		peer.create_server(54321, 2)
		create_player(1)
	else:
		print(ip)
		peer.create_client(ip, 54321) # The client is connected to the entered ip adress.
	
	get_tree().network_peer = peer # We initialize the peer of this Lobby with the created peer.
	get_tree().connect("connected_to_server", self, "_on_connected_to_server")
	get_tree().connect("connection_failed", self, "_on_connection_failed")
	get_tree().connect("network_peer_connected", self, "_on_network_peer_connected") 
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")

"""
This function creates the different players according to the number of players in the game.
"""
sync func set_players_(nb_player: int, decalage: int, id: int) -> void:
	var player
	for i in range(nb_player):
		if i+decalage == 0:
			player = load("res://Ressources/Players/BluePiece.tscn").instance()
			player.name = str(1)
			player.set_network_master(id)
			map_id_with_players[1] = player
		if i+decalage == 1:
			player = load("res://Ressources/Players/PinkPiece.tscn").instance()
			player.name = str(2)
			player.set_network_master(id)
			map_id_with_players[2] = player
		if i+decalage == 2:
			player = load("res://Ressources/Players/PurplePiece.tscn").instance()
			player.name = str(3)
			player.set_network_master(id)
			map_id_with_players[3] = player
		if i+decalage == 3:
			player = load("res://Ressources/Players/BrownPiece.tscn").instance()
			player.name = str(4)
			player.set_network_master(id)
			map_id_with_players[4] = player
		if i+decalage == 4:
			player = load("res://Ressources/Players/WhitePiece.tscn").instance()
			player.name = str(5)
			player.set_network_master(id)
			map_id_with_players[5] = player
		if i+decalage == 5:
			player = load("res://Ressources/Players/GreenPiece.tscn").instance()
			player.name = str(6)
			player.set_network_master(id)
			map_id_with_players[6] = player

"""
This function is called when a game is created only on one table.
"""
func create_player_solo(nb_player: int) -> void:
	set_players_(nb_player, 0, 1)
	game_loading = false

"""
This function is called when there is a new table connected. 'id' permits to identify which table called this function.
"""
func create_player(id: int) -> void:
	if id == 1:
		print("nb_players : "+ str(nb_players))
		set_table(nb_players, id)
		#rpc("set_players_", nb_players, 0, 1)
		#rpc executes the function on each peer of the network.
		if get_tree().network_peer:
			rpc_id(1, "new_player_added", map_id_with_table.size()) # We send to all peers that a new table joined the game
			pass
	else:
		print("nb_players : "+ str(nb_players))
		#rpc("set_players_", nb_players, map_id_with_players.size(), id)
		#rpc executes the function on each peer of the network.
		if get_tree().network_peer:
			rpc_id(1, "new_player_added", map_id_with_table.size())
			pass

remotesync func new_player_added(player_count: int) -> void:
	if MAX_PLAYER_COUNT == player_count:#if the client who called this function is the latest client who can join the game
		players_ready_count += 1
	if players_ready_count == MAX_PLAYER_COUNT:
		rpc("emit_game_ready")

"""
This function is called when the game is ready and informs every peers that the game can start.
"""
remotesync func emit_game_ready() -> void:
	emit_signal("game_ready")

sync func set_table(nb: int, id: int) -> void:
	map_id_with_nb[id] = nb

"""
This function is called when a client is connected to the server.
"""
func _on_connected_to_server() -> void:
	print("connected to server")
	create_player(get_tree().get_network_unique_id())
	rpc("set_players", nb_players)
	rpc("set_table", nb_players_end, get_tree().get_network_unique_id())
	if(!is_network_master()):
		print("map_id_with_players : "+str(map_id_with_players))

"""
This function is called when the client can not be connected to the network.
"""
func _on_connection_failed() -> void:
	print("connection failed")

"""
This function is called when a client or the server connect itself to the network.
"""
func _on_network_peer_connected(id: int) -> void:
	print("peer connected %s" %id)#id : it permits to identify the peer between every other peers
	create_player(id)
	print(map_id_with_players)

"""
This function is called when the server is disconnected from the network.
"""
func _on_server_disconnected() -> void:
	print("server disconnected")
	get_tree().network_peer = null

"""
This function is called when the table want to do a new game.
So we reinitiate all variables in order to create a new game.
"""
func new_game() -> void:
	# We discconect the peer from the network
	_on_server_disconnected()
	get_tree().disconnect("connected_to_server", self, "_on_connected_to_server")
	get_tree().disconnect("connection_failed", self, "_on_connection_failed")
	get_tree().disconnect("network_peer_connected", self, "_on_network_peer_connected")
	get_tree().disconnect("server_disconnected", self, "_on_server_disconnected")
	players_ready_count = 0
	map_id_with_players = {}
	map_id_with_table = {}
	is_server = null
	pass
