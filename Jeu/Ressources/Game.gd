extends Node2D
class_name Game


puppet var Board
var game_finished = false
var players = []
var current_player = 0
var game_loading = true
var game_started

var winner
puppet var ShakerDice:ShakerDice
var end_screen = true
var nb_appuyer = 0

var nb_players

var multi = false

var touched = false

var player_awarding = 0
var players_awards = false

"""
This function manages all the blocked cells to reduce the number of turn before they were unblocked.
When one of the blocked cells is no more blocked, we remove it from the list Board.blocked_cells.
"""
sync func cell_blocked_changes() -> void:
	for i in range(Board.blocked_cells.size()):
		Board.blocked_cells[i][1] -= 1
		if Board.blocked_cells[i][1] == 0:
			Board.blocked_cells[i][0].blocked = false
			Board.blocked_cells.remove(i)

"""
This function manages the turn of a player :
	- change the current player
	- call to dice_roll()
	- call to move()
"""
func player_turn() -> void:
	if multi:
		if(is_network_master()):
			Board.index_current_player = Board.index_current_player%(Board.players.size())
			Board.current_player = Board.players[Board.index_current_player]
			Board.index_current_player += 1
			var val = dice_roll()
			rpc("move", val, true, get_tree().get_network_unique_id())
		else:
			Board.index_current_player = Board.index_current_player%(Board.players.size())
			Board.current_player = Board.players[Board.index_current_player]
			Board.index_current_player += 1
			var val = dice_roll()
			rpc("move", val, false, get_tree().get_network_unique_id())
	else:
		Board.index_current_player = Board.index_current_player%(Board.players.size())
		Board.current_player = Board.players[Board.index_current_player]
		Board.index_current_player += 1
		var val = dice_roll()
		move_solo(val, true)

"""
This function manages movements of players.
is_network permits to know who called the function move and to adapt the function.
"""
sync func move(val: int, is_network: bool, id: int) -> void:
	if(is_network):
		# The ShakerDice shows the number of cases that the player will move on.
		ShakerDice.set_dice(val)
		if(!is_network_master()):
			Board.index_current_player = Board.index_current_player%(Board.players.size())
			Board.current_player = Board.players[Board.index_current_player]
			Board.index_current_player += 1
			Board.current_player.move(val)
		else:
			Board.current_player.move(val)
	else:
		ShakerDice.set_dice(val)
		if(is_network_master() or get_tree().get_network_unique_id() != id):
			# If the actual peer is not the same as the one who called this function we enter there
			Board.index_current_player = Board.index_current_player%(Board.players.size())
			Board.current_player = Board.players[Board.index_current_player]
			Board.index_current_player += 1
			Board.current_player.move(val)
		else:
			Board.current_player.move(val)

"""
This function is called when it is a solo game.
"""
func move_solo(val: int, is_network: bool) -> void:
	ShakerDice.set_dice(val)
	# If the actual peer is not the same as the one who called this function we enter there
	Board.current_player.move(val)

"""
This function permits to launch de dice to move the different players
"""
func dice_roll() -> int:
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var val = rng.randi_range(1,7+Board.current_player.change_dice_value)
	var tmp = val
	return val

"""
This function loads the board and the ShakerDice for the server
"""
func game_beginning() -> void:
	if multi:
		Board = load("res://Ressources/Board.tscn").instance()
		add_child(Board)
		var res_ = Board.creation(self)
		ShakerDice = load("res://Ressources/ShakerDice.tscn").instance()
		add_child(ShakerDice)
		ShakerDice.set_position(Vector2(800,800))
		rpc("set_game", res_)
		rpc("associate_player_table", Lobby.map_id_with_nb)
	else:
		Board = load("res://Ressources/Board.tscn").instance()
		add_child(Board)
		var res_ = Board.creation(self)
		ShakerDice = load("res://Ressources/ShakerDice.tscn").instance()
		# It loads the ShakerDice and place it in the center of the windows.
		add_child(ShakerDice)
		ShakerDice.set_position(Vector2(800,800))

sync func associate_player_table(map_id_with_nb):
	var j = 0
	for id in map_id_with_nb:
		for i in range(map_id_with_nb[id]):
			players[i+j].id = id
			Board.players[i+j].id = id
		j += map_id_with_nb[id]

"""
This function loads the board and the ShakerDice for the client
"""
sync func set_game(res: String) -> void:
	if(!is_network_master()):
		Board = load("res://Ressources/Board.tscn").instance()
		add_child(Board)
		Board.creation_client(self, res)
		ShakerDice = load("res://Ressources/ShakerDice.tscn").instance()
		# It loads the ShakerDice and place it in the center of the windows.
		add_child(ShakerDice)
		ShakerDice.set_position(Vector2(800,800))

"""
This is the function who takes Events on Screen and it launchs the player_turn()
"""
func _input(event) -> void:
	if(!game_loading):
		if(game_finished == false):
			if event is InputEventScreenTouch: # If a player touches the screen, this event will be raised.
				if event.is_pressed():
					if multi:
						if Board.players[Board.index_current_player%(Board.players.size())].id == get_tree().get_network_unique_id():
							# If it is a multiplayer game then every table will know that a player touched a table.
							player_turn()
							rpc("cell_blocked_changes")
					else:
						player_turn()
						cell_blocked_changes()

"""
This function is called when a player press the button 'Here' to inform on which table the player is.
"""
"""
sync func touched_(id: int) -> void:
	players[player_awarding].id = id
	touched = true
	player_awarding += 1
"""

"""
This function is called when the button Solo_Game is pressed.
It permit to load the waiting panel and to set all the game (players and board thanks to game_beginning)
"""
func start_solo_game() -> void:
	Lobby.start_solo(nb_players)
	multi = false
	$Panel.hide()
	$Waiting.show()
	
	# While the game is loading in the mode solo, the table is waiting otherwise it will raise an error.
	while(Lobby.game_loading):
		var t = Timer.new()
		t.set_wait_time(3)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
	
	print("Game is ready")
	
	var player_ids = Lobby.map_id_with_players.keys()
	player_ids.sort()
	
	for i in range(Lobby.map_id_with_players.size()):
		players.append(Lobby.map_id_with_players[player_ids[i]])
	
	var i = 0
	$Waiting.hide()
	game_beginning()
	game_loading = false


"""
This function is called when the button Create_Game or Join_Game are pressed.
It permit to load the waiting panel and to set all the game (players and board thanks to game_beginning)
"""
func start_game(as_server: bool) -> void:
	multi = true
	if(as_server):
		print("Server")
		# If the table is the server the waiting screen displays the different IPs addresses which are available on this table/computer.
		Lobby.set_players_end(nb_players)
		Lobby.start(as_server)
		var IPs = IP.get_local_addresses()
		var res = "Clients can connect on one of these following IPs addresses : \n"
		for i in range(len(IPs)):
			res += "- "+IPs[i]+"\n"
		$Waiting/IP_addresses.text = res
		$Waiting/Waiting_server.show()
	else:
		print("Client")
		Lobby.set_players_end(nb_players)
		Lobby.start_client(as_server, $Panel/IP.text)
		$Waiting/Waiting.show()
	$Panel.hide()
	$Waiting.show()
	
	# The peer is waiting while the game is not ready.
	yield(Lobby, "game_ready")
	
	print("Game is ready")
	
	var player_ids = Lobby.map_id_with_players.keys()
	player_ids.sort()
	
	for i in range(Lobby.map_id_with_players.size()):
		# This part loads every players on the array : players.
		players.append(Lobby.map_id_with_players[player_ids[i]])
	
	if(as_server):
		$Waiting/Waiting_server.hide()
	else:
		$Waiting/Waiting.hide()
	$Waiting.hide()
	if(is_network_master()):
		#rpc("players_awarding")
		game_beginning()
	game_loading = false

"""
This function matches each table to one or more players
Blue, Pink, Purple, Brown, White, Green
"""
sync func players_awarding() -> void:
	$Players.show()
	players_awards = true
	# Here, we tried to display each piece one after one in order to know on which table the player is.
	while(player_awarding < len(players)):
		if player_awarding == 0:
			$Players/BluePiece.show()
			while(players_awards):
				pass
			players_awards = true
			player_awarding += 1
			$Players/BluePiece.hide()
			touched = false
		elif player_awarding == 1:
			$Players/PinkPiece.show()
			while(players_awards):
				pass
			players_awards = true
			player_awarding += 1
			$Players/PinkPiece.hide()
			touched = false
		elif player_awarding == 2:
			$Players/PurplePiece.show()
			while(players_awards):
				pass
			players_awards = true
			player_awarding += 1
			$Players/PurplePiece.hide()
			touched = false
		elif player_awarding == 3:
			$Players/BrownPiece.show()
			while(players_awards):
				pass
			players_awards = true
			player_awarding += 1
			$Players/BrownPiece.hide()
			touched = false
		elif player_awarding == 4:
			$Players/WhitePiece.show()
			while(players_awards):
				pass
			players_awards = true
			player_awarding += 1
			$Players/WhitePiece.hide()
			touched = false
		elif player_awarding == 5:
			$Players/GreenPiece.show()
			while(players_awards):
				pass
			players_awards = true
			player_awarding += 1
			$Players/GreenPiece.hide()
			touched = false
	$Players.hide()
	print("Here")

"""
Functions about buttons
"""
func _on_Create_Game_pressed() -> void:
	start_game(true)

func _on_Join_Game_pressed() -> void:
	start_game(false)

func _on_Solo_Game_pressed() -> void:
	start_solo_game()

func _on_Play_pressed() -> void:
	nb_players = int($Panel2/nb_players.text)
	$Panel2.hide()
	$Panel.show()

"""
This function is called when the player wants to do another game.
So, we reinitialize every variables.
"""
func _on_Replay_pressed() -> void:
	$Replay.hide()
	$Panel2.show()
	#Reset of settings of the game.
	game_finished = false
	remove_child(Board)
	game_loading = true
	Lobby.new_game()
	Board = null
	players = []
	current_player = 0
	game_started = false
	ShakerDice = null
	winner = null


"""
This function manages the end of the game by showing final image.
It permits to show the replayer panel in order to play another game if players want.
"""
sync func end_game() -> void:
	game_finished = true
	winner = Board.current_player
	if winner.color_player == "Blue":
		$Final/BluePiece.show()
	if winner.color_player == "Brown":
		$Final/BrownPiece.show()
	if winner.color_player == "Pink":
		$Final/PinkPiece.show()
	if winner.color_player == "Purple":
		$Final/PurplePiece.show()
	if winner.color_player == "Green":
		$Final/GreenPiece.show()
	if winner.color_player == "White":
		$Final/WhitePiece.show()
	Board.hide()
	ShakerDice.hide()
	$Final.show()
	
	var t = Timer.new()
	t.set_wait_time(3)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	$Replay.show()
	$Final.hide()
	$Final/BluePiece.hide()
	$Final/BrownPiece.hide()
	$Final/PinkPiece.hide()
	$Final/PurplePiece.hide()
	$Final/GreenPiece.hide()
	$Final/WhitePiece.hide()

"""
This function is called when a player touched a screen when we assigned each player to a table.
"""
sync func touched_(id: int) -> void:
	players[player_awarding].id = id
	players_awards = false

func _on_Here_pressed() -> void:
	touched = true
	rpc("touched_", get_tree().get_instance_id())

"""
This function is called if the server wants to start the game before a 3rd client comes
"""
func _on_server_starts_pressed() -> void:
	Lobby.rpc("set_players_server", Lobby.nb_players)
	rpc("emit_game_ready")

sync func emit_game_ready() -> void:
	Lobby.emit_game_ready()
