extends AnimatedSprite
class_name Dice

var _rolling = true

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	prepare_dice()

#Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	"""
	if(_rolling):
		rotation_degrees = rotation_degrees+10
	pass
	"""

func prepare_dice() -> void:
	var rng = RandomNumberGenerator.new()
	var val = 0
	var prev = 0
	while(_rolling):
		var t = Timer.new()
		t.set_wait_time(0.1)
		t.set_one_shot(false)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		rng.randomize()
		while(val==prev):
			val = rng.randi_range(0,5) #TODO récupérer la modif du dé et l'ajouter à la var aléatoire
		if(_rolling):
			set_frame(val)
		prev = val
	var t2 = Timer.new()
	t2.set_wait_time(1)
	self.add_child(t2)
	t2.start()
	yield(t2, "timeout")
	self.remove_child(t2)

func set_rolling(rolling: bool) -> void:
	_rolling = rolling
#	if(rolling):
#		var t3 = Timer.new()
#		t3.set_wait_time(2)
#		self.add_child(t3)
#		t3.start()
#		yield(t3, "timeout")
#		_rolling=rolling
#		self.show()
#		prepare_dice()
#		self.remove_child(t3)
#	else:
#		_rolling=rolling
	pass

func set_dice(val: int) -> void:
	if(_rolling):
		_rolling=false
	var t4 = Timer.new()
	t4.set_wait_time(0.15)
	self.add_child(t4)
	t4.start()
	yield(t4, "timeout")
	set_frame(val-1)
	self.remove_child(t4)
	pass
