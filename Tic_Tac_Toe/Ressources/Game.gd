extends Node2D
class_name Game

var players = []
var Board
var current_player = 0
var game_finished = false
var game_load = true
var winner
var game_started
var positions = []

var new_Board
var new_positions = []
var server

onready var join = $Panel
onready var waiting_box = $Waiting
onready var final = $Final
onready var cross = $Final/cross_victory
onready var circle = $Final/circle_victory

sync func end_game(winner):
	Board.hide()
	final.show()
	if(winner == "circle"):
		circle.show()
	else:
		cross.show()

var nb_appuyer = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func game_beginning():
	Board = load("res://Ressources/Board.tscn").instance()
	Board.set_game(self)
	add_child(Board)
	game_started = true
	var cross = load("res://Ressources/Player_cross.tscn").instance()
	var circle = load("res://Ressources/Player_circle.tscn").instance()

func _on_Join_Game_pressed() -> void:
	start_game(false)

func start_game(as_server):
	if(as_server):
		print("Server")
		Lobby.start(as_server)
	else:
		print("Client")
		Lobby.start_client(as_server, $Panel/IP.text)
	server = as_server
	join.hide()
	waiting_box.show()
	
	yield(Lobby, "game_ready")
	
	print("Game is ready")
	
	var player_ids = Lobby.map_id_with_players.keys()
	player_ids.sort()
	
	var i = 0
	waiting_box.hide()
	game_beginning()
	game_load = false
	for id in player_ids:
		var player = Lobby.map_id_with_players[id]
		if i == 0:
			player.texture = load("res://Ressources/Circle.tscn").instance()
		else:
			player.texture = load("res://Ressources/Cross.tscn").instance()
		players.append(player)
		i += 1


func _on_Create_Game_pressed():
	start_game(true)
