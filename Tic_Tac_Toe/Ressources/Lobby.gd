extends Node

var is_server

var map_id_with_players = {}
var player_packed = preload("res://Ressources/Player.tscn")

const MAX_PLAYER_COUNT = 2
var players_ready_count = 0

signal game_ready

func start(as_server = false) -> void:
	is_server = as_server
	
	var peer = NetworkedMultiplayerENet.new()
	
	if(is_server):
		peer.create_server(54321, 2)
		print(IP.get_local_addresses())
		
		create_player(1)
	else:
		peer.create_client("localhost", 54321)
	
	get_tree().network_peer = peer
	get_tree().connect("connected_to_server", self, "_on_connected_to_server")
	get_tree().connect("connection_failed", self, "_on_connection_failed")
	get_tree().connect("network_peer_connected", self, "_on_network_peer_connected") #quand un autre élément du réseau s'est connecté à nous
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")

func start_client(as_server = false, ip = "localhost") -> void:
	is_server = as_server
	
	var peer = NetworkedMultiplayerENet.new()
	
	if(is_server):
		peer.create_server(54321, 2)
		create_player(1)
	else:
		print(ip)
		peer.create_client(ip, 54321)
	
	get_tree().network_peer = peer
	get_tree().connect("connected_to_server", self, "_on_connected_to_server")
	get_tree().connect("connection_failed", self, "_on_connection_failed")
	get_tree().connect("network_peer_connected", self, "_on_network_peer_connected") #quand un autre élément du réseau s'est connecté à nous
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")

func create_player(id):
	if id == 1:
		var player = load("res://Ressources/Player_circle.tscn").instance()
		player.name = str(id)
		player.set_network_master(id)
		map_id_with_players[id] = player
		#rpc execute sur tous les peer d'un réseau0
		if get_tree().network_peer:
			rpc_id(1, "new_player_added", map_id_with_players.size())
	else:
		var player = load("res://Ressources/Player_cross.tscn").instance()
		player.name = str(id)
		player.set_network_master(id)
		map_id_with_players[id] = player
		#rpc execute sur tous les peer d'un réseau0
		if get_tree().network_peer:
			rpc_id(1, "new_player_added", map_id_with_players.size())

remotesync func new_player_added(player_count):#remotesync : dit à godot qu'elle doit etre executée à distance
	if MAX_PLAYER_COUNT == player_count:#joueur qui a notifié le serveur est le dernier
		players_ready_count += 1
	if players_ready_count == MAX_PLAYER_COUNT:
		rpc("emit_game_ready")

remotesync func emit_game_ready():
	emit_signal("game_ready")

func _on_connected_to_server():
	print("connected to server")
	create_player(get_tree().get_network_unique_id())

func _on_connection_failed():
	print("connection failed")

func _on_network_peer_connected(id):
	print("peer connected %s" %id)#id : identifiant unique assigné par godot au peer qui s'est connecté
	create_player(id)

func _on_server_disconnected():
	print("server disconnected")
	get_tree().network_peer = null
