extends Node2D
class_name Board

var current_player = 0
var Game
var winner
puppet var positions = []
puppet var positions_string = []

var new_board
var new_positions_circle = []
var new_positions_cross = []

onready var board = $Board_fin

sync func has_in_positions(position_x, position_y):
	return Game.players[current_player].has_in_positions(position_x, position_y)

sync func end_game(winner):
	Game.end_game(winner)

sync func add_circle(position_x, position_y):
	if(current_player == 0):	
		if positions[position_x][position_y] == null:
			Game.players[current_player].add_circle(position_x, position_y)
			var new_circle = load("res://Ressources/Circle.tscn").instance()
			add_child(new_circle)
			new_circle.position = Vector2(position_x*64, position_y*64)
			positions[position_x][position_y] = new_circle
			positions_string[position_x][position_y] = "circle"
			current_player += 1
			current_player = current_player % Game.players.size()

sync func add_cross(position_x, position_y):
	if(current_player == 1):
		if positions[position_x][position_y] == null:
			Game.players[current_player].add_cross(position_x, position_y)
			var new_cross = load("res://Ressources/Cross.tscn").instance()
			add_child(new_cross)
			new_cross.position = Vector2(position_x*64, position_y*64)
			positions[position_x][position_y] = new_cross
			positions_string[position_x][position_y] = "cross"
			current_player += 1
			current_player = current_player % Game.players.size()

sync func test_end_game():
	if(positions_string[0][0] != null):
		if(positions_string[0][2] != null):
			if(positions_string[0][4] != null):
				if positions_string[0][0] == positions_string[0][2] and positions_string[0][0] == positions_string[0][4] :
					winner = Game.players[current_player]
					Game.game_finished = true
					rpc("end_game", positions_string[0][0])
		if(positions_string[2][0] != null):
			if(positions_string[4][0] != null):
				if positions_string[0][0]  == positions_string[2][0]  and positions_string[0][0]  == positions_string[4][0] :
					winner = Game.players[current_player]
					Game.game_finished = true
					rpc("end_game", positions_string[0][0])
		if(positions_string[2][2] != null):
			if(positions_string[4][4] != null):
				if positions_string[0][0] == positions_string[2][2] and positions_string[2][2] == positions_string[4][4]:
					winner = Game.players[current_player]
					Game.game_finished = true
					rpc("end_game", positions_string[0][0])
	if(positions_string[2][0] != null):
		if(positions_string[2][2] != null):
			if(positions_string[2][4] != null):
				if positions_string[2][4] == positions_string[2][2] and positions_string[2][4] == positions_string[2][0]:
					winner = Game.players[current_player]
					Game.game_finished = true
					rpc("end_game", positions_string[2][4])
	if(positions_string[4][0] != null):
		if(positions_string[4][2] != null):
			if(positions_string[4][4] != null):
				if positions_string[4][4] == positions_string[4][0] and positions_string[4][0] == positions_string[4][2]:
					winner = Game.players[current_player]
					Game.game_finished = true
					rpc("end_game", positions_string[4][4])
		if(positions_string[2][2] != null):
			if(positions_string[0][4] != null):
				if positions_string[2][2] == positions_string[4][0] and positions_string[2][2] == positions_string[0][4]:
					winner = Game.players[current_player]
					Game.game_finished = true
					rpc("end_game", positions_string[0][4])
	if(positions_string[0][2] != null):
		if(positions_string[2][2] != null):
			if(positions_string[4][2] != null):
				if positions_string[2][2] == positions_string[4][2] and positions_string[0][2] == positions_string[2][2]:
					winner = Game.players[current_player]
					Game.game_finished = true
					rpc("end_game", positions_string[2][2])
	if(positions_string[0][4] != null):
		if(positions_string[2][4] != null):
			if(positions_string[4][4] != null):
				if positions_string[2][4] == positions_string[4][4] and positions_string[2][4] == positions_string[0][4]:
					winner = Game.players[current_player]
					Game.game_finished = true
					rpc("end_game", positions_string[2][4])
	pass

var nb_appuyer = 0

func set_game(new_game):
	positions.append([null, null, null, null, null])
	positions.append([null, null, null, null, null])
	positions.append([null, null, null, null, null])
	positions.append([null, null, null, null, null])
	positions.append([null, null, null, null, null])
	
	positions_string.append([null, null, null, null, null])
	positions_string.append([null, null, null, null, null])
	positions_string.append([null, null, null, null, null])
	positions_string.append([null, null, null, null, null])
	positions_string.append([null, null, null, null, null])
	Game = new_game

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if(Game != null):
		if(Game.game_load == false):
			if is_network_master():
				rpc_unreliable("update_game", Game.players)
			else:
				pass
				
	pass

sync func player_turn(position_x: float, position_y: float):
	if current_player == 0:
		if Game.server:
			rpc("add_circle", position_x, position_y)
	else:
		if !Game.server:
			rpc("add_cross", position_x, position_y)

puppet func update_game(players):#executée sur ceux qui suivent le master
	new_positions_circle = Game.players[0].positions
	new_positions_cross = Game.players[1].positions

func _input(event):
	if(!Game.game_load):
		if(Game.game_finished == false):
			if event is InputEventScreenTouch:
				if event.is_pressed():
					if event.position.x < 320 and event.position.y < 320 and event.position.x > 0 and event.position.y > 0 :
						if event.position.x < 64 :
							if event.position.y < 64:
								player_turn(0,0)
								rpc("test_end_game")
							elif event.position.y > 128 and event.position.y < 192:
								player_turn(0,2)
								rpc("test_end_game")
							elif event.position.y > 256:
								player_turn(0,4)
								rpc("test_end_game")
						elif event.position.x > 128 and event.position.x < 192:
							if event.position.y < 64:
								player_turn(2,0)
								rpc("test_end_game")
							elif event.position.y > 128 and event.position.y < 192:
								player_turn(2,2)
								rpc("test_end_game")
							elif event.position.y > 256:
								player_turn(2,4)
								rpc("test_end_game")
						elif event.position.x > 256:
							if event.position.y < 64:
								player_turn(4,0)
								rpc("test_end_game")
							elif event.position.y > 128 and event.position.y < 192:
								player_turn(4,2)
								rpc("test_end_game")
							elif event.position.y > 256:
								player_turn(4,4)
								rpc("test_end_game")
	pass
