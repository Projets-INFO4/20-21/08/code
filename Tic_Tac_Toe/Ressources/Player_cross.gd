extends Node2D
class_name Player_cross

var positions = []
var new_positions = []
var texture
var object

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

sync func add_cross(positionx : float, positiony: float):
	positions.append([positionx,positiony])
	object = "cross"

func get_object():
	return object

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_network_master():
		rpc_unreliable("update_game", positions)
	pass

puppet func update_game(_new_positions):#executée sur ceux qui suivent le master
	new_positions = _new_positions

sync func has_in_positions(position_x, position_y):
	for i in range(positions.size()):
		print(positions[i])
		print([position_x,position_y])
		if positions[i] == [position_x,position_y]:
			return true
	return false
